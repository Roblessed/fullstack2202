<?php
    //Declara una variable (datos de entrada)
    $str = "Mary Had A Little Lamb and She LOVED It So";
    // Proceso
    $str = strtoupper($str);
    //salida
    echo $str .'<br>'; // muestra: MARY HAD A LITTLE LAMB AND SHE LOVED IT SO

    //Ejemplo de AND &&
    $edad = 19;
    $sexo = 'F';
    //Necesitamos verificar que sea mayor de 18 y el sexo F
    if( $edad > 18 || $sexo == 'M' ){
        //Imprimo leyenda
        //Uso de comillas simples con variable php
        echo 'Es una mujer que tiene $edad años'.'<br>';
        //Uso de comillas simples con variable php y concatenando
        echo 'Es una mujer que tiene '.$edad.' años'.'<br>';
        //Uso de comillas doble con variable php
        echo "Es una mujer que tiene $edad <br><br>";
    }
    //Algoritmo para sumar los numeros del 1 al 10
    $numero = 0;
    $suma = 0;
    echo "El valor inicial de suma es: $suma  y el número inicial es: $numero<br>";

    while ($numero <= 10){
        $suma += $numero;
        echo "La suma parcial es: $suma <br>";
        //$suma = $suma + $numero;
        $numero++;
        echo "El número nuevo es: $numero <br><br>";
    }
    echo "El resultado de la suma es: $suma";



?>
 